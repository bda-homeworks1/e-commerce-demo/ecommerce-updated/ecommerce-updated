package com.example.ecommerceupdated;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommerceUpdatedApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceUpdatedApplication.class, args);
	}

}
