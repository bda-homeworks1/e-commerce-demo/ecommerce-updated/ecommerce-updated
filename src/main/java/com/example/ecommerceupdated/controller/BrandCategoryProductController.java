package com.example.ecommerceupdated.controller;

import com.example.ecommerceupdated.entity.BrandCategoryProductEntity;
import com.example.ecommerceupdated.service.BrandCategoryProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/data")
public class BrandCategoryProductController {
    @Autowired
    BrandCategoryProductService brandCategoryProductService;

    @GetMapping("/get-all")
    public List<BrandCategoryProductEntity> getDatas(){
       return brandCategoryProductService.getData();
    }

    @PostMapping("/create")
    public String setData(){
        return brandCategoryProductService.setData();
    }
}
