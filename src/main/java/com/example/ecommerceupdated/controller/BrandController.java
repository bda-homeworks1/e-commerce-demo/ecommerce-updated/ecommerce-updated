package com.example.ecommerceupdated.controller;


import com.example.ecommerceupdated.entity.BrandEntity;
import com.example.ecommerceupdated.service.impl.BrandServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    BrandServiceImpl brandService ;

    @GetMapping("/get-all")
    public List<BrandEntity> getBrands(){ return brandService.getBrands(); }

    @PostMapping("/create")
    public String setBrand(){ return brandService.setBrand(); }

}
