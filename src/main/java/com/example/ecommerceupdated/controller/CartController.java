package com.example.ecommerceupdated.controller;

import com.example.ecommerceupdated.entity.CartEntity;
import com.example.ecommerceupdated.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cart")
public class CartController {
    @Autowired
    CartService cartService;

    @GetMapping("/get-all")
    public List<CartEntity> getCarts(){
        return cartService.getCarts();
    }

    @PostMapping("/create")
    public String setCart(){
        return cartService.setCart();
    }


}
