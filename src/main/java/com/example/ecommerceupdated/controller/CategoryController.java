package com.example.ecommerceupdated.controller;

import com.example.ecommerceupdated.entity.CategoryEntity;
import com.example.ecommerceupdated.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping("/get-all")
    public List<CategoryEntity> getCategories(){
        return categoryService.getCategories();
    }

    @PostMapping("/create")
    public String setCategory(){
        return categoryService.setCategory();
    }
}
