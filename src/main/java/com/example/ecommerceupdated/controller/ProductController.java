package com.example.ecommerceupdated.controller;

import com.example.ecommerceupdated.entity.ProductEntity;
import com.example.ecommerceupdated.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/get-all")
    public List<ProductEntity> getProducts(){
        return productService.getProducts();
    }

    @PostMapping("/create")
    public String setProduct(){
        return productService.setProduct();
    }
}
