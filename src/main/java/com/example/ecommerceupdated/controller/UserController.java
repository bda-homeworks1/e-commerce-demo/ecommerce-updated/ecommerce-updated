package com.example.ecommerceupdated.controller;

import com.example.ecommerceupdated.entity.UserEntity;
import com.example.ecommerceupdated.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/get-all")
    public List<UserEntity> getUsers(){
        return userService.getUsers();
    }

    @PostMapping("/create")
    public String setUser(){
        return userService.setUser();
    }
}
