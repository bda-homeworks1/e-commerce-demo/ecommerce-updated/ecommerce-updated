package com.example.ecommerceupdated.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BrandCategoryProductModel {
    Long id;
    Long brandId;
    Long categoryId;
    Long productId;

}
