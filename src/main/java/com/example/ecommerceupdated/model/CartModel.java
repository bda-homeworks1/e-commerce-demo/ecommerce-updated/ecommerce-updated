package com.example.ecommerceupdated.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CartModel {
    Long id;
    Long data_id;
    Long user_id;
    Long count;
    Double totalAmount;
}
