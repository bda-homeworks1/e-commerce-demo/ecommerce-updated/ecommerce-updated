package com.example.ecommerceupdated.model;


import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductModel {
    Long id;
    String name;
    Double amount;
    Long remainCount;
    String details;
}
