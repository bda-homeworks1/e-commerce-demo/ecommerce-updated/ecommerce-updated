package com.example.ecommerceupdated.model;


import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserModel {
    Long id;
    String name;
    String surname;
    String username;
    String password;
    String birthdate;
    String email;
    String address;
}
