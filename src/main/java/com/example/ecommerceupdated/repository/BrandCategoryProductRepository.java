package com.example.ecommerceupdated.repository;

import com.example.ecommerceupdated.entity.BrandCategoryProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandCategoryProductRepository extends JpaRepository<BrandCategoryProductEntity, Long> {
}
