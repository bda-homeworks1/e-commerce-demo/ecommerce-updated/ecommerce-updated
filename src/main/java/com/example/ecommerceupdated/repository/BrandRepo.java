package com.example.ecommerceupdated.repository;


import com.example.ecommerceupdated.entity.BrandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandRepo extends JpaRepository<BrandEntity, Long> {

}
