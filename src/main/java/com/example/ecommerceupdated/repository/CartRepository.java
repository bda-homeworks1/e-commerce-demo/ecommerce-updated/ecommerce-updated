package com.example.ecommerceupdated.repository;

import com.example.ecommerceupdated.entity.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<CartEntity, Long> {
}
