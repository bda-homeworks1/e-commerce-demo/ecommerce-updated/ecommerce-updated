package com.example.ecommerceupdated.repository;


import com.example.ecommerceupdated.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {
}
