package com.example.ecommerceupdated.repository;

import com.example.ecommerceupdated.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
}
