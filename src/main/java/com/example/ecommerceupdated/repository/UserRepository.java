package com.example.ecommerceupdated.repository;

import com.example.ecommerceupdated.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
}
