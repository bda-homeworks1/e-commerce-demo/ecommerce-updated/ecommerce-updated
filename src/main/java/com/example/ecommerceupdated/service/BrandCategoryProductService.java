package com.example.ecommerceupdated.service;

import com.example.ecommerceupdated.entity.BrandCategoryProductEntity;

import java.util.List;

public interface BrandCategoryProductService {
    List<BrandCategoryProductEntity> getData();
    String setData();
}
