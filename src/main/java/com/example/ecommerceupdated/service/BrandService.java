package com.example.ecommerceupdated.service;

import com.example.ecommerceupdated.entity.BrandEntity;

import java.util.List;

public interface BrandService {

     List<BrandEntity> getBrands();
     String setBrand();

}
