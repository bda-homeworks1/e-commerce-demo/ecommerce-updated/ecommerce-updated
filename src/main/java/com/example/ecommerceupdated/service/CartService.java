package com.example.ecommerceupdated.service;

import com.example.ecommerceupdated.entity.CartEntity;

import java.util.List;

public interface CartService {
    List<CartEntity> getCarts();
    String setCart();
}
