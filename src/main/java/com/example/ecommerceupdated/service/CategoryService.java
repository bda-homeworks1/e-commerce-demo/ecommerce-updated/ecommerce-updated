package com.example.ecommerceupdated.service;


import com.example.ecommerceupdated.entity.CategoryEntity;

import java.util.List;

public interface CategoryService {
    List<CategoryEntity> getCategories();
    String setCategory();
}
