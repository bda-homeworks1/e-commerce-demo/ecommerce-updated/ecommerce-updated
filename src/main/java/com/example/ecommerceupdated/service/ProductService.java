package com.example.ecommerceupdated.service;

import com.example.ecommerceupdated.entity.ProductEntity;

import java.util.List;

public interface ProductService {
    List<ProductEntity> getProducts();
    String setProduct();
}
