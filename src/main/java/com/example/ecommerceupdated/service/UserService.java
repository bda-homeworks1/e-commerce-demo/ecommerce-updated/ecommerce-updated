package com.example.ecommerceupdated.service;

import com.example.ecommerceupdated.entity.UserEntity;

import java.util.List;

public interface UserService {
    List<UserEntity> getUsers();
    String setUser();
}
