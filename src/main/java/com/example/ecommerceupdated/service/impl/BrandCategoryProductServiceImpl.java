package com.example.ecommerceupdated.service.impl;

import com.example.ecommerceupdated.entity.BrandCategoryProductEntity;
import com.example.ecommerceupdated.entity.BrandEntity;
import com.example.ecommerceupdated.entity.CategoryEntity;
import com.example.ecommerceupdated.entity.ProductEntity;
import com.example.ecommerceupdated.repository.BrandCategoryProductRepository;
import com.example.ecommerceupdated.repository.BrandRepository;
import com.example.ecommerceupdated.repository.CategoryRepository;
import com.example.ecommerceupdated.repository.ProductRepository;
import com.example.ecommerceupdated.service.BrandCategoryProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
public class BrandCategoryProductServiceImpl implements BrandCategoryProductService {
    static Scanner scanner = new Scanner(System.in);
    @Autowired
    BrandCategoryProductRepository brandCategoryProductRepository;

    @Autowired
    BrandRepository brandRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ProductRepository productRepository;

    public List<BrandCategoryProductEntity> getData(){
        return brandCategoryProductRepository.findAll();
    }

    public String setData(){
        System.out.println("-------------------------");
        System.out.println("Data elave edilmesi:");
        System.out.println("-------------------------");

        System.out.println("Brend id:");
        Long brandId = scanner.nextLong();
        Optional<BrandEntity> optionalBrand = brandRepository.findById(brandId);
        String brandName = optionalBrand.stream().map(item-> item.getName()).toString();
        BrandEntity brand = new BrandEntity(brandId,brandName,null);

        System.out.println("Kategory id:");
        Long categoryId = scanner.nextLong();
        Optional<CategoryEntity> optionalCategory = categoryRepository.findById(categoryId);
        String categoryName = optionalCategory.stream().map(item -> item.getName()).toString();
        CategoryEntity category = new CategoryEntity(categoryId, categoryName);

        System.out.println("Product id:");
        Long productId = scanner.nextLong();
        Optional<ProductEntity> optionalProduct = productRepository.findById(productId);
        String productName = optionalProduct.stream().map(item -> item.getName()).toString();
        Double productAmount = Double.parseDouble(optionalProduct.stream().map(item -> item.getAmount()).toString());
        Long productRemainCount = Long.parseLong(optionalProduct.stream().map(item -> item.getRemainCount()).toString());
        String productDetails = optionalProduct.stream().map(item -> item.getDetails()).toString();
        ProductEntity product = new ProductEntity(productId,productName,productAmount,productRemainCount,productDetails);

        BrandCategoryProductEntity brandCategoryProduct = new BrandCategoryProductEntity(null,brand,category,product);
        brandCategoryProductRepository.save(brandCategoryProduct);
        return "Data elave edildi";
    }
}
