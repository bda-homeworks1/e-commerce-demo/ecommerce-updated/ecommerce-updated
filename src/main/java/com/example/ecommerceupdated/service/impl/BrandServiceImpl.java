package com.example.ecommerceupdated.service.impl;

import com.example.ecommerceupdated.entity.BrandEntity;
import com.example.ecommerceupdated.repository.BrandRepository;
import com.example.ecommerceupdated.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;


@Service
public class BrandServiceImpl implements BrandService {
    static Scanner scanner = new Scanner(System.in);

    @Autowired
    BrandRepository brandRepository;

    public List<BrandEntity> getBrands(){
         return brandRepository.findAll() ;
    }

    public String setBrand(){
        System.out.println("-------------------------");
        System.out.println("Brend elave edilmesi:");
        System.out.println("-------------------------");
        System.out.println("ad:");
        String name = scanner.next();
        System.out.println("desc:");
        String desc = scanner.next();
        if(name!=null){
            BrandEntity brand = new BrandEntity(null,name,desc);
            return "Brend elave edildi";
        }else{
            return "Ad elave etmek mutleqdir!";
        }

    }
}
