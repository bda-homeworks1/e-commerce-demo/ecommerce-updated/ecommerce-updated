package com.example.ecommerceupdated.service.impl;

import com.example.ecommerceupdated.entity.*;
import com.example.ecommerceupdated.repository.*;
import com.example.ecommerceupdated.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
public class CartServiceImpl implements CartService {
    static Scanner scanner = new Scanner(System.in);

    @Autowired
    CartRepository cartRepository;

    @Autowired
    BrandCategoryProductRepository brandCategoryProductRepository;

    @Autowired
    BrandRepository brandRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    UserRepository userRepository;

    public List<CartEntity> getCarts(){
        return cartRepository.findAll();
    }

    public String setCart(){
        System.out.println("-------------------------");
        System.out.println("Sebete mehsul elave edilmesi:");
        System.out.println("-------------------------");
        System.out.println("Data id:");
        Long dataId = scanner.nextLong();

        Optional<BrandCategoryProductEntity> optionalBrandCategoryProduct = brandCategoryProductRepository.findById(dataId);
        Long remainCount = Long.parseLong(optionalBrandCategoryProduct.stream().map(item -> item.getProduct().getRemainCount()).toString());

        if(remainCount>0){
            Long brandId = Long.parseLong(optionalBrandCategoryProduct.stream().map(item-> item.getBrand().getId()).toString());
            String brandName = optionalBrandCategoryProduct.stream().map(item -> item.getBrand().getName()).toString();
            BrandEntity brand = new BrandEntity(brandId,brandName,null);

            Long categoryId = Long.parseLong(optionalBrandCategoryProduct.stream().map(item -> item.getCategory().getId()).toString());
            String categoryName = optionalBrandCategoryProduct.stream().map(item -> item.getCategory().getName()).toString();
            CategoryEntity category = new CategoryEntity(categoryId,categoryName);

            Long productId = Long.parseLong(optionalBrandCategoryProduct.stream().map(item -> item.getProduct().getId()).toString());
            String productName = optionalBrandCategoryProduct.stream().map(item -> item.getProduct().getName()).toString();
            Double amount = Double.parseDouble(optionalBrandCategoryProduct.stream().map(item -> item.getProduct().getAmount()).toString());
            String productData = optionalBrandCategoryProduct.stream().map(item -> item.getProduct().getDetails()).toString();
            ProductEntity product = new ProductEntity(productId,productName,amount,remainCount,productData);

            BrandCategoryProductEntity brandCategoryProduct = new BrandCategoryProductEntity(dataId,brand,category,product);

            System.out.println("user id:");
            Long userId = scanner.nextLong();
            Optional<UserEntity> optionalUser = userRepository.findById(userId);
            String name = optionalUser.stream().map(item -> item.getName()).toString();
            String surname = optionalUser.stream().map(item -> item.getSurname()).toString();
            String username = optionalUser.stream().map(item -> item.getUsername()).toString();
            String password = null;
            String birthdate = null;
            String email = optionalUser.stream().map(item -> item.getEmail()).toString();
            String address = optionalUser.stream().map(item -> item.getAddress()).toString();
            UserEntity user = new UserEntity(userId,name,surname,username,password,birthdate,null,email,address);

            System.out.println("say:");
            Long totalCount = scanner.nextLong();
            Double totalAmount = totalCount * amount;

            CartEntity cart = new CartEntity(null,brandCategoryProduct,user,totalCount,totalAmount);
            cartRepository.save(cart);
            return "Mehsul sebete elave olundu";
        }else{
            return "Bu mehsuldan anbarda qalmayib";
        }



    }
}
