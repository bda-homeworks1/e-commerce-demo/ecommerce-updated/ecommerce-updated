package com.example.ecommerceupdated.service.impl;


import com.example.ecommerceupdated.entity.CategoryEntity;
import com.example.ecommerceupdated.repository.CategoryRepository;
import com.example.ecommerceupdated.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;

@Service
public class CategoryServiceImpl implements CategoryService {
    static Scanner scanner = new Scanner(System.in);
    @Autowired
    CategoryRepository categoryRepository;

    public List<CategoryEntity> getCategories(){
        return categoryRepository.findAll();
    }

    public String setCategory(){
        System.out.println("-------------------------");
        System.out.println("Kategoriya elave edilmesi:");
        System.out.println("-------------------------");
        System.out.println("Ad:");
        String name = scanner.next();
        if(name!=null){
            CategoryEntity category = new CategoryEntity(null, name);
            categoryRepository.save(category);
            return "Kategoriya elave olundu";
        }else{
            return "Ad elave etmek mutleqdir!";
        }
    }

}
