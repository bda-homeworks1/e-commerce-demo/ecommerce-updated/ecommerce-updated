package com.example.ecommerceupdated.service.impl;

import com.example.ecommerceupdated.entity.ProductEntity;
import com.example.ecommerceupdated.repository.ProductRepository;
import com.example.ecommerceupdated.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;

@Service
public class ProductSeviceImpl implements ProductService {
    static Scanner scanner = new Scanner(System.in);

    @Autowired
    ProductRepository productRepository;

    public List<ProductEntity> getProducts(){
        return productRepository.findAll();
    }

    public String setProduct(){
        System.out.println("-------------------------");
        System.out.println("Product elave edilmesi:");
        System.out.println("-------------------------");
        System.out.println("Ad:");
        String name = scanner.next();
        System.out.println("Qiymet:");
        Double price = scanner.nextDouble();
        System.out.println("Nece eded qalib?");
        Long remainCount = scanner.nextLong();
        System.out.println("Mehsul haqqinda detallar:");
        String details = scanner.next();
        if(name!=null && price!=null && remainCount!=null ){
            ProductEntity productEntity = new ProductEntity(null,name,price,remainCount,details);
            productRepository.save(productEntity);
            return "Product elave edildi";
        }else {
            return "Ad, Qiymet, Qalma Sayi bos qala bilmez";
        }
    }
}
