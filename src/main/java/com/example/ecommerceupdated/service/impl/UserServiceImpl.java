package com.example.ecommerceupdated.service.impl;

import com.example.ecommerceupdated.entity.UserEntity;
import com.example.ecommerceupdated.repository.UserRepository;
import com.example.ecommerceupdated.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

@Service
public class UserServiceImpl implements UserService {
    static Scanner scanner = new Scanner(System.in);
    @Autowired
    UserRepository userRepository;

    public List<UserEntity> getUsers(){
        return userRepository.findAll();
    }

    public String setUser(){
        System.out.println("-------------------------");
        System.out.println("Istifadeci elave edilmesi:");
        System.out.println("-------------------------");
        System.out.println("Ad:");
        String name = scanner.next();
        System.out.println("Soyad:");
        String surname = scanner.next();
        System.out.println("Username:");
        String username = scanner.next();
        System.out.println("Password:");
        String password = scanner.next();
        System.out.println("Ad gunu:");
        String birthdate = scanner.next();
        String createDate = LocalDate.now().toString();
        System.out.println("Email:");
        String email = scanner.next();
        System.out.println("Address:");
        String address = scanner.next();

        UserEntity user = new UserEntity(null,name,surname,username,password,birthdate,createDate,email,address);
        userRepository.save(user);
        return "User elave edildi";
    }
}
